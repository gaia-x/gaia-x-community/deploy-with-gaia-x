This project provides GitLab CI job templates for deploying K8S apps from
GitLab with [Krake](https://gitlab.com/rak-n-rok/krake). It also comes with a
demo.

## Job API of `templates.yml`

### `.build-and-push-docker-image-template`

Build a Dockerfile and push the image to a registry.

Required variables:
- `REGISTRY`: url of the docker image registry
- `REGISTRY_USER`
- `REGISTRY_PASSWORD`
- `REGISTRY_IMAGE`: full path of the docker image to create
- `IMAGE_TAG`: additional tag for the docker image to create
  (e.g. `latest`, `$CI_COMMIT_SHA`)

Requires GitLab Runner 11.2 or later in order to work.

### `.deploy-K8S-app-with-krake-template`

Deploy a K8S manifest with Krake. The manifest will be pre-processed with
some text substitutions (see below). Overwrite `before_script` to supply
credentials required by `rok` or to make the Krake service accessible.
A previous deployment with the same name will be updated, if possible.

Required variables:
- `ROK_YAML_PATH`
- `K8S_MANIFEST_PATH`
- `KRAKE_K8S_APP_NAME`

Optional variables:
- `KRAKE_K8S_LABEL_CONSTRAINTS`: `;` delimited label constraints like
  `location is DE;<label> != <value>;<label> in (<value>, <value>)` that
  will be passed to the `rok kube app create` or `rok kube app update` command
- `KRAKE_K8S_RESOURCE_CONSTRAINTS`: similar to `KRAKE_K8S_LABEL_CONSTRAINTS` but
  with resource constraints specified like `<plural>.<group>;<plural2>.<group>2`
- `KRAKE_K8S_APP_OPTIONS`: additional option string passed to the
  `rok kube app create` or `rok kube app update` command (e.g. hooks or
  namespace options)

The optional manifest pre-processing will replace the following strings:
- `$IMAGE` with `$REGISTRY_IMAGE`
- `$TAG` with `$IMAGE_TAG`
- `$AUTH_STRING` with a computed value from `$REGISTRY`, `$REGISTRY_USER`,
   and `$REGISTRY_PASSWORD`

See https://rak-n-rok.readthedocs.io/projects/krake/en/latest/user/rok-documentation.html
and https://rak-n-rok.readthedocs.io/projects/krake/en/latest/dev/scheduling.html#constraints
for further details regarding relevant command-line options and the constraint syntax.

## How to use the template in your project

Prepare at least the following files:
- `Dockerfile`
- `rok.yaml`
- `manifest.yaml`

Build the `rok.Dockerfile` and make the image accessible to your GitLab Runner.
It provides an execution environment for the deploy job.

Include the job templates like this:

```yml
include:
- project: '<URL_TO_THIS_GITLAB_PROJECT>'
  ref: master
  file: '/template.yml'

build-and-push-docker-image:
  stage: build
  extends: .build-and-push-docker-image-template
  ...

deploy-via-krake:
  stage: deploy
  extends: .deploy-K8S-app-with-krake-template
  image: <MY_ROK_IMAGE>
  ...
```

See https://docs.gitlab.com/ee/ci/yaml/#include for other include options.

## Demo

The demo (see `demo/.gitlab-ci.yml`) builds a Docker image from `demo/Dockerfile`
containing nginx and a website and pushs it to the private GitLab Docker image
registry of this project. It uses SSH port forwarding to make the Krake server
accessible. It finally deploys a processed `demo/manifest.yaml` via `rok` with
TLS authentication (as configured in `demo/rok.yaml`).

Some proteced variables with the type `File` were defined in the GitLab UI to
make this work:
- `CA_PEM`, `SYSTEM_ADMIN_KEY_PEM`, `SYSTEM_ADMIN_PEM`: certificates for TLS
- `ROK_GATEWAY_HOST`: SSH private key of a (very restricted) port forwarding user
- `KNOWN_HOSTS`

The files `demo/Dockerfile`, `demo/demo.conf` and `demo/index.html` are derived
with minor changes from https://github.com/nginxinc/NGINX-Demos/tree/master/nginx-hello-nonroot/html-version,
which was released under the [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0.html).

## Useful resources

- https://rak-n-rok.readthedocs.io/projects/krake/en/latest/user/configuration.html#rok-configuration
- https://rak-n-rok.readthedocs.io/projects/krake/en/latest/user/rok-documentation.html#the-application-resource-app
- https://gitlab.com/guided-explorations/containers/kaniko-docker-build
  - uses variables grouped by environments for advanced (re)usage
  - supports multiple docker image tags of variable length
