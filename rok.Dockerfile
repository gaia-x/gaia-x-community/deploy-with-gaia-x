FROM python:3.6-slim
RUN apt-get update && apt-get install -y --no-install-recommends git ssh
RUN git clone --depth=1 https://gitlab.com/rak-n-rok/krake.git
RUN pip3 install /krake/rok
RUN mkdir /etc/rok
CMD ["/bin/bash"]
